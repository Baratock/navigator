import nav.NavData;

import java.util.Random;

/**
 * Created by barat on 18.04.2016.
 */
public class RouteBuilder {

	private float startlat;
	private float startlong;
	private int crossings;
	private NavData navi;

	public RouteBuilder(NavData navi) {
		this.startlat = 49.488776f;
		this.startlong = 11.153693f;
		this.crossings = 5;
		this.navi = navi;
	}

	public RouteBuilder withStartPointAt(float x, float y) {
		this.startlat = x;
		this.startlong = y;
		return this;
	}

	public RouteBuilder withCrossingCount(int count) {
		this.crossings = count;
		return this;
	}

	private int GetNearestCrossing(float latitute, float longitute) {
		int factor = (int) Math.pow(10, 6);
		return navi.getNearestCrossing((int) (latitute * factor), (int) (longitute * factor));
	}

	public PathHelper[] Build() {
		int start = GetNearestCrossing(startlat, startlong);
		PathHelper[] route = new PathHelper[crossings];
		Random r = new Random();
		for (int i = 0; i < crossings; i++) {
			int node;
			int link;
			//Knoten
			if (i == 0) {
				node = start;
			} else {
				node = navi.getCrossingIDTo(route[i - 1].getNext());
			}

			//Links zum nächsten
			if (i != crossings - 1) {
				int[] links = navi.getLinksForCrossing(node);
				int j = r.nextInt(links.length);
				link = links[j];
			} else {
				link = -1;
			}
			route[i] = new PathHelper(node, link);
		}
		return route;
	}


}
