import nav.NavData;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by barat on 13.04.2016.
 */
public class CommandWriterTest {


	public static NavData navi;

	@BeforeClass
	public static void InitializeNavDataForTest() throws Exception {
		navi = new NavData("res/CAR_CACHE_de_noCC_mittelfranken.CAC", true);
	}

	private int GetNearestCrossing(float latitute, float longitute) {
		int factor = (int) Math.pow(10, 6);
		return navi.getNearestCrossing((int) (latitute * factor), (int) (longitute * factor));
	}


	@Test
	public void _0DegShouldReturnCommandForGoingStraight() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", 0);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung geradeaus", result);
	}

	@Test
	public void _5DegShouldReturnCommandForGoingStraight() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", 5);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung geradeaus", result);
	}

	@Test
	public void minus5DegShouldReturnCommandForGoingStraight() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", -5);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung geradeaus", result);
	}

	@Test
	public void minus6DegShouldReturnCommandForASlightRightTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", -6);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung leicht Rechts abbiegen in dumdum", result);
	}

	@Test
	public void minus40DegShouldReturnCommandForASlightRightTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", -40);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung leicht Rechts abbiegen in dumdum", result);
	}


	@Test
	public void _6DegShouldReturnCommandForASlightRightTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", 6);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung leicht Links abbiegen in dumdum", result);
	}

	@Test
	public void _40DegShouldReturnCommandForASlightRightTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", 40);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung leicht Links abbiegen in dumdum", result);
	}

	@Test
	public void _41DegShouldReturnCommandForLeftTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", 41);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung Links abbiegen in dumdum", result);
	}


	@Test
	public void _110DegShouldReturnCommandForLeftTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", 110);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung Links abbiegen in dumdum", result);
	}

	@Test
	public void minus41DegShouldReturnCommandForRightTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", -41);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung Rechts abbiegen in dumdum", result);
	}


	@Test
	public void minus110DegShouldReturnCommandForRightTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", -110);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung Rechts abbiegen in dumdum", result);
	}

	@Test
	public void minus111DegShouldReturnCommandForSharpRightTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", -111);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung scharf Rechts abbiegen in dumdum", result);
	}

	@Test
	public void _111DegShouldReturnCommandForSharpLeftTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", 111);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung scharf Links abbiegen in dumdum", result);
	}

	@Test
	public void minus170DegShouldReturnCommandForSharpRightTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", -170);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung scharf Rechts abbiegen in dumdum", result);
	}

	@Test
	public void _170DegShouldReturnCommandForSharpLeftTurn() {
		//arrange
		CommandWriter cw = new CommandWriter(null);
		CrossingHelper helper = new CrossingHelper("dum", "dumdum", 170);

		//act
		String result = cw.commandByAngleAndName(helper);

		//assert
		assertEquals("An der naechsten Kreuzung scharf Links abbiegen in dumdum", result);
	}

	@Test
	public void GetAngleShouldReturnAnAngle()
	{
		//arrange
		PathHelper[] route = aRoute().withStartPointAt(49.453992f, 11.073299f).withCrossingCount(3).Build();
		CommandWriter cw = new CommandWriter(navi);

		//act
		int result = cw.getAngle(route[0].getNext(), route[1].getNext());

		System.out.println("angle: " + result);

		//assert
		assertThat(result, is(not(0)));
	}

	private RouteBuilder aRoute() {
		return new RouteBuilder(navi);
	}
}