import com.vividsolutions.jts.util.Stopwatch;
import fu.keys.LSIClassCentre;
import nav.NavData;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by barat on 11.04.2016.
 */
public class NaviUtil {

    NavData navi;

    public NaviUtil(NavData navi) {
        this.navi = navi;
    }

    public PathHelper[] getRoute(int fromID, int toID) {
        Astar star = new Astar(navi);
        ArrayList<PathHelper> helpers = star.run(fromID, toID);


        PathHelper[] path = ToPath(helpers);
        System.out.println("Route: " + path.length + " Looked: " + star.getCounter());
        System.out.println("Avg Open: " + star.openCount());
        return path;
    }


    //Converting the Crossing+Previous-Format form Astar to a Crossing-Next Format
    private PathHelper[] ToPath(ArrayList<PathHelper> helpers) {
        int size = helpers.size();
        PathHelper[] path = new PathHelper[size];
        int index = 0;
        for (PathHelper helper : helpers) {
            path[size - 1 - index] = helper;
            index++;
        }
        return path;
    }

    //Generating commands
    public void printCommands(PathHelper[] route) throws FileNotFoundException, UnsupportedEncodingException {
        CommandWriter writer = new CommandWriter(navi);
        writer.run(route);
    }


    //Generating Routeprinting in Dorenda
    public void printRoute(PathHelper[] route) {
        RoutePrinter printer = new RoutePrinter(navi);
        printer.run(route);
    }


    //Print time of Route
    public void printTime(PathHelper[] route) {
        int x = 0;
        Astar astar = new Astar(navi);
        for (PathHelper p : route) {
            if (p.getNext() != -1)
                x += astar.getCostOf(p.getNext());
        }
        System.out.println("Time is " + x / 3600 + ":" + (x / 60) % 60 + ":" + x % 60 + "h");
    }
}
