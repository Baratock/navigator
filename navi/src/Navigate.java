import nav.NavData;

import java.io.IOException;

public class Navigate {
    public static void main(String[] args) throws IOException {
        // Pause for profiling
        //System.in.read();

        String mappath = args[0];
        float startlat = Float.parseFloat(args[1]);
        float startlong = Float.parseFloat(args[2]);
        float targetlat = Float.parseFloat(args[3]);
        float targetlong = Float.parseFloat(args[4]);

        int multiplier = (int) Math.pow(10, 6);

        NavData navi;
        try {
            navi = new NavData(mappath, true);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }


        //Serious Business starts here!!
        int idFrom = navi.getNearestCrossing((int) (startlat * multiplier), (int) (startlong * multiplier));
        int idTo = navi.getNearestCrossing((int) (targetlat * multiplier), (int) (targetlong * multiplier));

        NaviUtil util = new NaviUtil(navi);
        PathHelper[] route = util.getRoute(idFrom, idTo);

        util.printTime(route);
        //util.printRoute(route);
        util.printCommands(route);

        System.out.println("Finished!");
        util.printRoute(route);
    }
}