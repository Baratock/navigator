import nav.NavData;
import pp.dorenda.client2.additional.UniversalPainterWriter;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

/**
 * Created by barat on 11.04.2016.
 */
public class RoutePrinter {
    NavData navi;
    double[] Lats = {};
    double[] Longs = {};
    java.lang.String outtext = "Route.txt";

    public RoutePrinter(NavData navi) {
        this.navi = navi;
    }

    public void run(PathHelper[] route) {

        GetLatsLongs(route);
        connectToDorenda();
    }

    private void connectToDorenda() {
        try {
            pp.dorenda.client2.additional.UniversalPainterWriter dorenda = new pp.dorenda.client2.additional.UniversalPainterWriter(outtext);
            dorenda.line(Lats, Longs, 0, 255, 0, 200, 4, 3, "Start", "...Route...", "End");
            dorenda.close();
            System.out.println("Dorenda success!");
        } catch (Exception e) {
            System.out.print("dorenda: Creation of map failed in RoutePrinter");
            System.out.print(e.getMessage());
        }
    }

    private void GetLatsLongs(PathHelper[] route) {
        Lats = new double[route.length];
        Longs = new double[route.length];
        double factor = 1000000;
        int i = 0;

        for (PathHelper ph :
                route) {
            Lats[i] = (double) navi.getCrossingLatE6(ph.getNode()) / factor;
            Longs[i] = (double) navi.getCrossingLongE6(ph.getNode()) / factor;

            i++;
        }
    }
}
