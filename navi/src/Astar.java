import com.vividsolutions.jts.util.Stopwatch;
import fu.geo.Spherical;
import nav.NavData;

import java.util.*;

/**
 * Created by Andreas on 13.04.2016.
 * Skript: 175
 */
public class Astar implements Comparator<Integer> {

    private NavData navi;
    private int counter = 0;
    private long opened = 0;
    private PriorityQueue<Integer> OpenList;
    private boolean[] CheckOpen;
    private boolean[] closed;
    private int[] previous;

    private int End;

    private int[] f;
    private int[] g;


    //Build Astar Objeczt
    public Astar(NavData navi) {
        this.navi = navi;
        int crossings = navi.getCrossingCount();
        f = new int[crossings];
        g = new int[crossings];


        OpenList = new PriorityQueue<>(this);

        closed = new boolean[crossings];
        CheckOpen = new boolean[crossings];

        previous = new int[crossings];


        for (int i = 0; i < crossings; i++) {
            f[i] = (Integer.MAX_VALUE);
            g[i] = (Integer.MAX_VALUE);
            previous[i] = -2;
        }
    }

    //Run the Astar
    public ArrayList<PathHelper> run(int start, int target) {
        Stopwatch sw = new Stopwatch();
        System.out.println("Start Routing");
        sw.start();

        AstarAlgorithm2(start, target);

        sw.stop();
        System.out.println("Finish Routing with " + sw.getTimeString());

        System.out.println("Process Route...");
        ArrayList<PathHelper> path = processRoute(target);

        System.out.println("Finish Processing");
        return path;
    }


    //Process Route for Output
    private ArrayList<PathHelper> processRoute(int target) {
        ArrayList<PathHelper> path = new ArrayList<>();
        //Endheper with End and path = -1 //First should be (First, pathtosecond)
        path.add(new PathHelper(target, -1));


        int node = target;
        int pre = 0;
        //while (previous.get(node) != -1) {
        //pre = previous.get(node);
        while (previous[node] != -1) {
            pre = previous[node];
            int nextLink = getLinkBetweenCrossings(pre, node);
            path.add(new PathHelper(pre, nextLink));
            node = pre;
        }
        return path;
    }

    //Astar itself
    private boolean AstarAlgorithm2(int start, int target) //h, c
    {

        //start.Besuch
        updateNode(start);
        this.End = target;
        //previous.put(start, -1);
        previous[start] = -1;
        g[start] = 0;

        while (OpenList.size() > 0) {
            int currentPoint = pollNode();
            //opened+= OpenList.size();
            counter++;
            if (currentPoint == target)
                return true;
            expand2(currentPoint);
            closed[currentPoint] = true;
        }
        return false;
    }

    //Expand
    private void expand2(int currentPoint) {
        //foreach neighbour from currentPoint
        List<Integer> neighbourLinks = getNeighbourLinks(currentPoint);
        int[] neighbours = getNeighbours(neighbourLinks);

        int g_akt;
        int f_akt;

        int i = 0;
        for (int n : neighbours) {
            if (inClosed(n))
                continue;
            g_akt = g[currentPoint] + getCostOf(neighbourLinks.get(i));
            f_akt = g_akt + h(n, End);

            if (inOpen(n)) {
                if (f_akt >= f[n])
                    continue;
                removeNode(n);
            }
            //previous.put(n, currentPoint);
            previous[n] = currentPoint;
            g[n] = g_akt;
            f[n] = f_akt;

            updateNode(n);
            //if(neighbour is closed) : continue !!!!
            i++;
        }

    }


    //--- Helpfunctions for expand
    private void removeNode(int n) {
        OpenList.remove(n);
        CheckOpen[n] = false;
    }

    private boolean inOpen(int n) {
        return CheckOpen[n];
    }

    private Integer pollNode() {
        int n = OpenList.poll();
        CheckOpen[n] = false;
        return n;
    }

    private boolean inClosed(int n) {
        return closed[n];
    }

    private void updateNode(int n) {
        OpenList.add(n);
        CheckOpen[n] = true;

    }

    private int[] getNeighbours(List<Integer> neighbourLinks) {
        int j = 0;
        int[] neighbours = new int[neighbourLinks.size()];
        for (int i : neighbourLinks) {
            neighbours[j] = navi.getCrossingIDTo(i);
            j++;
        }
        return neighbours;
    }

    private List<Integer> getNeighbourLinks(int currentPoint) {
        int[] neighbourLinks = navi.getLinksForCrossing(currentPoint);
        List<Integer> links = new LinkedList<>();
        for (int j : neighbourLinks) {
            if (navi.goesCounterOneway(j))
                continue;
            links.add(j);
        }
        return links;
    }

    private int getLinkBetweenCrossings(int cur, int tar) {
        int links[] = navi.getLinksForCrossing(cur);
        int targets[] = navi.getLinksForCrossing(tar);

        int link = 0;
        for (int i : links) {
            for (int j : targets) {
                if (i == j) {
                    if (!navi.goesCounterOneway(i)) {
                        link = i;
                        break;
                    }
                } else if (navi.getReverseLink(j) == i) {
                    link = i;
                    break;
                }
            }
        }
        return link;
    }


    //Costfunction of Road
    public int getCostOf(int linkID) {
        int lengthMeters = navi.getLengthMeters(linkID);
        int speedLimit = getSpeedLimit(linkID);
        return (lengthMeters * 3600) / (speedLimit * 1000);
    }

    private int getSpeedLimit(int linkID) {
        int speedLimit = navi.getMaxSpeedKMperHours(linkID);
        if (speedLimit == 0) {
            //String LSIToken = LSIClassCentre.lsiClassByID(navi.getLSIclass(linkID)).classToken;

            int LSIToken = navi.getLSIclass(linkID);

            speedLimit = getSpeedLimitByToken(LSIToken);
        }
        return speedLimit;
    }

    private int getSpeedLimitByToken(int LSIToken) {
        int speedLimit;
        switch (LSIToken) {
            case 32711000:
                speedLimit = 10;
                break;
            case 34110000:
                speedLimit = 130;
                break;
            case 34141000:
                speedLimit = 50;
                break;
            case 34142000:
                speedLimit = 30;
                break;
            case 34176000:
                speedLimit = 20;
                break;
            default:
                speedLimit = 100;
                break;
        }
        return speedLimit;
    }

    private int h(int target, int element) {
        //skyway is the highway :-P
        double factor = 1000000D;
        int lat1 = navi.getCrossingLatE6(target);
        int lat2 = navi.getCrossingLatE6(element);
        int long1 = navi.getCrossingLongE6(target);
        int long2 = navi.getCrossingLongE6(element);

        double distance = Spherical.greatCircleMeters(lat1 / factor, long1 / factor, lat2 / factor, long2 / factor);

        int seconds = (int) (distance / 125) * 3600 / 1000;
        return seconds;
    }

    public int getCounter() {
        return counter;
    }


    //Compare function for for PriorityQueue
    @Override
    public int compare(Integer o1, Integer o2) {
        if (f[o1] < f[o2]) {
            return -1;
        } else {
            return 1;
        }
    }

    public long openCount() {
        return opened / counter;
    }
}
