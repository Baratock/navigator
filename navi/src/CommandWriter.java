import fu.keys.LSIClassCentre;
import nav.NavData;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.Objects;

/**
 * Created by barat on 11.04.2016.
 */
public class CommandWriter {
    NavData navi;
    private boolean noTurnFlag;

    public CommandWriter(NavData navi) {
        noTurnFlag = false;
        this.navi = navi;
    }

    public void run(PathHelper[] route) throws FileNotFoundException, UnsupportedEncodingException {
        LinkedList<String> commands = createCommandList(route);
        writeComandList(commands);
    }

    private void writeComandList(LinkedList<String> commands) throws FileNotFoundException, UnsupportedEncodingException {
        java.io.PrintWriter writer = new java.io.PrintWriter("Turns.txt", "UTF-8");
        for (String comand : commands) {
            //System.out.println(comand);
            writer.println(comand);
        }
        writer.close();
    }

    public LinkedList<String> createCommandList(PathHelper[] route) {
        int end = route.length - 1;
        LinkedList<String> list = new LinkedList<String>();
        list.add(createStartCommand(route[0]));
        //Route ab der ersten Kreuzung zum Ziehl
        for (int i = 1; i < end; i++) {
            int fromLink = route[i - 1].getNext();
            int toLink = route[i].getNext();
            CrossingHelper ch = buildCrossingHelper(fromLink, toLink);
            addCommandToList(list, ch);
            if (i == end - 1) {
                list.add(finishCommand(ch));
            }
        }
        return list;

    }

    private String finishCommand(CrossingHelper ch) {
        String name = ch.getTodomain();
        return "Sie haben ihr Ziel in " + name + " erreicht!";
    }

    private void addCommandToList(LinkedList<String> list, CrossingHelper helper) {
        if (helper.getFromdomain() != null && helper.getTodomain() != null) {
            if (helper.getFromdomain().equals(helper.getTodomain()) && noTurnFlag) {
                return;
            }
        }
        String command = commandForCrossing(helper);
        list.add(command);
    }

    private String createStartCommand(PathHelper pathHelper) {
        String domain = navi.getDomainName(navi.getDomainID(pathHelper.getNext()));
        return "Fahr beginnen auf " + domain;
    }

    private CrossingHelper buildCrossingHelper(int fromLink, int toLink) {
        int angle = getAngle(fromLink, toLink);
        String from = navi.getDomainName(navi.getDomainID(fromLink));
        String to = navi.getDomainName(navi.getDomainID(toLink));
        CrossingHelper crossingHelper = new CrossingHelper(from, to, angle);
        crossingHelper.setFromLSIToken(navi.getLSIclass(fromLink));
        crossingHelper.setToLSIToken(navi.getLSIclass(toLink));
        return crossingHelper;
    }

    public int getAngle(int fromLink, int toLink) {

        int a1 = navi.getNorthAngleFrom(fromLink);
        int a2 = navi.getNorthAngleFrom(toLink);

        int angle = (a2 - a1);
        if (angle > 180)
            angle -= 360;
        if (angle < -180)
            angle += 360;
        return angle;
    }

    public String commandForCrossing(CrossingHelper helper) {
        if (helper.getFromdomain() != null && helper.getTodomain() != null) {
            if (helper.getFromdomain().equals(helper.getTodomain())) {
                noTurnFlag = true;
                return "Dem Straßenverlauf folgen";
            }
        }
        return commandByAngleAndName(helper);
    }

    public String commandByAngleAndName(CrossingHelper helper) {
        noTurnFlag = false;
        String name = helper.getTodomain();
        if (name == null || Objects.equals(name, "")) {
            name = LSIClassCentre.lsiClassByID(helper.getToLSIToken()).className;
        }
        int angle = helper.getAngle();

        if (angle >= -5 && angle <= 5)
            return "An der naechsten Kreuzung geradeaus auf " + name;
        if (angle >= 6 && angle <= 40)
            return "An der naechsten Kreuzung leicht Links abbiegen in " + name;
        if (angle <= -6 && angle >= -40)
            return "An der naechsten Kreuzung leicht Rechts abbiegen in " + name;
        if (angle >= 41 && angle <= 110)
            return "An der naechsten Kreuzung Links abbiegen in " + name;
        if (angle <= -41 && angle >= -110)
            return "An der naechsten Kreuzung Rechts abbiegen in " + name;
        if (angle >= 111 && angle <= 170)
            return "An der naechsten Kreuzung scharf Links abbiegen in " + name;
        if (angle <= -111 && angle >= -170)
            return "An der naechsten Kreuzung scharf Rechts abbiegen in " + name;
        return "";
    }


}
