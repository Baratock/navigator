/**
 * Created by barat on 20.04.2016.
 */
public class PathHelper {
    private int node;
    private int next;

    public PathHelper(int node, int next) {
        this.node = node;
        this.next = next;
    }

    public int getNext() {
        return next;
    }

    public int getNode() {
        return node;
    }
}
