/**
 * Created by barat on 13.04.2016.
 */
public class CrossingHelper {
    private String fromdomain;
    private String todomain;
    private int angle;
    private int toLSIToken;
    private int fromLSIToken;

    public CrossingHelper(String fromdomain, String todomain, int angle) {
        this.fromdomain = fromdomain;
        this.todomain = todomain;
        this.angle = angle;
    }

    public String getFromdomain() {
        return fromdomain;
    }

    public String getTodomain() {
        return todomain;
    }

    public int getAngle() {
        return angle;
    }

    public int getFromLSIToken() {
        return fromLSIToken;
    }

    public void setFromLSIToken(int fromLSIToken) {
        this.fromLSIToken = fromLSIToken;
    }

    public int getToLSIToken() {
        return toLSIToken;
    }

    public void setToLSIToken(int toLSIToken) {
        this.toLSIToken = toLSIToken;
    }
}
